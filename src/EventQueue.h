#ifndef _EVENT_QUEUE_
#define _EVENT_QUEUE_

#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/joystick.h>
#include <boost/atomic.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/lockfree/queue.hpp>

namespace joytype {

  class EventQueue
  { 
  public:
    typedef boost::lockfree::queue<js_event> queue_type;
    typedef boost::shared_ptr<queue_type> shared_ptr;
  };
}

#endif
