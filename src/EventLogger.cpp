#include <iostream>
#include "EventLogger.h"

using namespace joytype;

void EventLogger::operator()(const js_event& e)
{
  std::cout << "Time: " << (int) e.time
            << " Type: " << (int) e.type
	    << " num: "  << (int) e.number
	    << " val: " << e.value << std::endl; 
}
