#ifndef _ANALOG_STICK_
#define _ANALOG_STICK_

#include "joystick.h"

namespace joytype
{
  class AnalogStick
  {
    struct cartesian_coords
    {
      int x;
      int y;
    };

    struct polar_coords
    {
      double mod;
      double arg;
    };

  public:
    void operator()(const js_event&);

  private:
    void calc_polar(void);

  private:
    cartesian_coords _cartesian;
    polar_coords _polar;

  };
}
#endif
