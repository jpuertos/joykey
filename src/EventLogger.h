#ifndef _EVENT_LOGGER_
#define _EVENT_LOGGER_

#include "joystick.h"

namespace joytype {
  class EventLogger
  {
  public:
    void operator()(const js_event&);
  };
  
}

#endif
