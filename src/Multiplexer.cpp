#include "Multiplexer.h"

using namespace joytype;

void EventMultiplexer::operator()(const js_event& e)
{
    switch (e.type)
    {
    case JS_EVENT_BUTTON:
      _button_signal->operator()(e);
      break;

    case JS_EVENT_AXIS:
      _axis_signal->operator()(e);
      break;
    }
}

void EventMultiplexer::connect_axis(EventNotifier::slot_type slot)
{
  _axis_signal->connect(slot);

}

void EventMultiplexer::connect_button(EventNotifier::slot_type slot)
{
  _button_signal->connect(slot);

}
