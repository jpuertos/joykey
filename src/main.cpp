#include <boost/thread.hpp>

#include "joystick.h"
#include "EventLogger.h"
#include "EventQueue.h"
#include "EventNotifier.h"
#include "Multiplexer.h"
#include "AnalogStick.h"

// g++ -ggdb -std=c++11 *.cpp -lboost_thread -o j
int main()
{
  joytype::EventQueue::shared_ptr q_ptr(new joytype::EventQueue::queue_type(256));

  joytype::Joystick js;
  //joytype::AnalogStick stick;
  joytype::EventNotifier notifier(q_ptr);
  joytype::EventMultiplexer mux;


  notifier.connect(mux);
  //mux.connect_axis(joytype::EventLogger());
  mux.connect_axis(joytype::AnalogStick());


  js.open("/dev/input/js0", q_ptr);
  // sleep(0.2);

  boost::thread joy_t(js);
  boost::thread notifier_t(notifier);
  
   while(true)
    {
      boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
    }

  joy_t.join();
  notifier_t.join();
}
