#include <iostream>
#include <math.h>
#include <limits>
#include "AnalogStick.h"
#include <boost/algorithm/clamp.hpp>

using namespace joytype;

void AnalogStick::operator()(const js_event& e)
{
  switch (e.number)
    {
    case 0:
      _cartesian.x = e.value;
      break;
    case 1:
      _cartesian.y = e.value;
      break;
    }
  calc_polar();
  std::cout << _polar.mod << ", " << _polar.arg << std::endl;
}

void AnalogStick::calc_polar(void)
{
  _polar.mod = sqrt(_cartesian.x * _cartesian.x + 
		    _cartesian.y * _cartesian.y) / std::numeric_limits<short signed>::max();


  _polar.mod = boost::algorithm::clamp(_polar.mod, 0.0, 1.0);
  _polar.arg = atan2(_cartesian.y, _cartesian.x);
}
