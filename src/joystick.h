#ifndef _JOYSTICK_
#define _JOYSTICK_

#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/joystick.h>

#include <vector>
#include "EventQueue.h"

namespace joytype
{

  class Joystick
  {
  public:
    Joystick();
    int open(const std::string&);
    int open(const std::string&, EventQueue::shared_ptr);
    int close(void);
    void operator()(void);

  private:
    int _fd;
    EventQueue::shared_ptr _queue;
  };

}

#endif

/*
 1. Histogram of most frequent words.
 2. Table prefix -> next letter, prefix -> complete word.
 */
