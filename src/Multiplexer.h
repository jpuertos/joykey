#ifndef _EVENT_MULTIPLEXER_
#define _EVENT_MULTIPLEXER_

#include "EventNotifier.h"

namespace joytype {
  class EventMultiplexer
  {
  public:

  EventMultiplexer() : _axis_signal(new EventNotifier::signal_type),
      _button_signal(new EventNotifier::signal_type)
	{}
    
    void operator()(const js_event&);
    void connect_axis(EventNotifier::slot_type);
    void connect_button(EventNotifier::slot_type);
    
  private:
    boost::shared_ptr<EventNotifier::signal_type> _axis_signal;
    boost::shared_ptr<EventNotifier::signal_type> _button_signal;
  };
  
}

#endif
