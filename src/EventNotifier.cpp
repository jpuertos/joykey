#include <boost/thread.hpp>
#include "EventNotifier.h"

using namespace joytype;

EventNotifier::EventNotifier(EventQueue::shared_ptr q) : _signal(new signal_type)
{
  _queue = q;
}

void EventNotifier::connect(slot_type slot)
{
  _signal->connect(slot);

}

void EventNotifier::operator()(void)
{
  EventQueue::queue_type::value_type e;
  while(true)
    {
      while(_queue->pop(e))
	{
	  _signal->operator()(e);
	  // std::cout << ": " << e.type << ", " << e.number << ", " << e.value << std::endl; 
	}
      boost::this_thread::sleep_for(boost::chrono::milliseconds(1));
    }
}
