#include <unistd.h>
#include <sys/select.h>
#include <boost/thread.hpp>
#include "joystick.h"

using namespace joytype;

Joystick::Joystick()
{
  _fd = -1;
}

int Joystick::open(const std::string& dev)
{
  _fd = ::open(dev.c_str(), O_RDONLY | O_NONBLOCK);
  return _fd;
}

int Joystick::open(const std::string& dev, EventQueue::shared_ptr q)
{
  open(dev);
  _queue = q;
}

int Joystick::close(void)
{
  return ::close(_fd);
}

void Joystick::operator()(void)
{
  struct js_event event;
   while(true)
    {
      while (read(_fd, &event, sizeof(struct js_event)) > 0)
	{
	  _queue->push(event);
	}
      
      if (errno != EAGAIN) // EAGAIN is returned when the queue is empty.
	 { 
	   // TODO: Process actual error!
	 }
      boost::this_thread::sleep_for(boost::chrono::microseconds(10000));
    }
}
