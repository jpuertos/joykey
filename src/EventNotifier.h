#ifndef _EVENT_NOTIFIER_
#define _EVENT_NOTIFIER_

#include <boost/function.hpp>
#include <boost/signals2.hpp>
#include <boost/shared_ptr.hpp>
#include "EventQueue.h"

namespace joytype {
  class EventNotifier
  {
  public:
    typedef boost::function<void (const js_event&)> slot_type;
    typedef boost::signals2::signal<void (const js_event&)> signal_type;

    EventNotifier(EventQueue::shared_ptr);
    void operator()(void);
    void connect(slot_type);

  private:
    EventQueue::shared_ptr _queue;
    boost::shared_ptr<signal_type> _signal;
  };
  
}

#endif

